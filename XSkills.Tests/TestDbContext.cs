﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace XSkills.Tests
{
    class TestDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseInMemoryDatabase("test");
        }

        public DbSet<TestModel> TestModels { get; set; }

        public DbSet<TestItem> TestItems { get; set; }
    }

    class TestModel
    {
        public TestModel()
        {
            TestItems = new HashSet<TestItem>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual IEnumerable<TestItem> TestItems { get; set; }
    }

    class TestItem
    {
        public int Id { get; set; }

        public int Value { get; set; }

        public string Key { get; set; }

        public virtual TestModel TestModel { get; set; }
        public virtual int TestModelId { get; set; }
    }
}