using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using XSkills.Web.Services;

namespace XSkills.Tests
{
    [TestClass]
    public class AnalyticsServiceTests
    {
        private const string Test = "test";
        private TestDbContext db;

        [TestMethod]
        public void CanRegisterOneToOneMap()
        {
            var service = new AnalyticsService<TestDbContext>();
            service.RegisterMapSingle(Test, x => x.TestModels.ToDictionary(k => k.Name, v => v.TestItems.Count().ToString()), DisplayType.List);
            var map = service.GetMap(Test);
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void CanRegisterOneToManyMap()
        {
            var service = new AnalyticsService<TestDbContext>();
            service.RegisterMap(Test, x => x.TestModels.ToDictionary(k => k.Name, v => Enumerable.Range(0, v.TestItems.Count()).Select(y => y.ToString())), DisplayType.List);
            var map = service.GetMap(Test);
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void CanConvertMapNameToCamelCase()
        {
            var service = new AnalyticsService<TestDbContext>();
            service.RegisterMapSingle("name to convert", x => x.TestModels.ToDictionary(k => k.Name, v => v.Name), DisplayType.List);
            service.RegisterMapSingle("nospaces", x => x.TestModels.ToDictionary(k => k.Name, v => v.Name), DisplayType.List);
            service.RegisterMapSingle("with a single letter", x => x.TestModels.ToDictionary(k => k.Name, v => v.Name), DisplayType.List);

            var map = service.GetMap("name to convert");
            Assert.AreEqual("nameToConvert", map.NameCamelCase);
            map = service.GetMap("nospaces");
            Assert.AreEqual("nospaces", map.NameCamelCase);
            map = service.GetMap("with a single letter");
            Assert.AreEqual("withASingleLetter", map.NameCamelCase);
        }

        [TestMethod]
        public void CanExecuteOneToOneMap()
        {
            InitializeDb();
            var service = new AnalyticsService<TestDbContext>();
            service.RegisterMapSingle(Test, x => x.TestModels.ToDictionary(k => k.Name, v => v.TestItems.FirstOrDefault().Value.ToString()), DisplayType.List);

            var map = service.GetMap(Test);
            var result = map.EvaluateSingle(db);
            var emptyResult = map.EvaluateCollection(db);
            Assert.IsNull(emptyResult);
            Assert.AreEqual("1", result.Values.First());
            db.Database.EnsureDeleted();
        }

        [TestMethod]
        public void CanExecuteOneToManyMap()
        {
            InitializeDb();
            var service = new AnalyticsService<TestDbContext>();
            service.RegisterMap(Test, x => x.TestModels.ToDictionary(k => k.Name, v => v.TestItems.Select(t => t.Key)), DisplayType.List);

            var map = service.GetMap(Test);
            var result = map.EvaluateCollection(db);
            var emptyResult = map.EvaluateSingle(db);
            Assert.IsNull(emptyResult);
            Assert.AreEqual(3, result.Values.First().Count());
            db.Database.EnsureDeleted();
        }

        private void InitializeDb()
        {
            var db = new TestDbContext();
            var model = new TestModel { Name = "test model" };
            model.TestItems = new[] {
                new TestItem { Key= "one", Value = 1, TestModel = model },
                new TestItem { Key= "two", Value = 2, TestModel = model },
                new TestItem { Key= "three", Value = 3, TestModel = model },
            };
            db.TestModels.Add(model);
            db.SaveChanges();

            this.db = db;
        }
    }
}
