using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using XSkills.Web.Models;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication;

namespace XSkills.Web.Pages.Account
{
    [AllowAnonymous]
    public class LogInModel : PageModel
    {
        private readonly SignInManager<XSkillsUser> signInManager;
        private readonly IOptions<XSkillsConfig> config;

        [BindProperty]
        public LogInFormModel Form { get; set; } = new LogInFormModel();

        public bool RequireConfirmedEmail { get; }

        public IEnumerable<AuthenticationScheme> ExternalLogins { get; set; }

        public LogInModel(SignInManager<XSkillsUser> signInManager, IOptions<XSkillsConfig> config)
        {
            this.signInManager = signInManager;
            this.config = config;
            RequireConfirmedEmail = config.Value.RequireConfirmedEmail;
        }

        public async Task OnGetAsync()
        {
            ExternalLogins = await signInManager.GetExternalAuthenticationSchemesAsync();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl)
        {
            ExternalLogins = await signInManager.GetExternalAuthenticationSchemesAsync();
            if (!ModelState.IsValid)
            {
                Form.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                return Page();
            }

            var result = await signInManager.PasswordSignInAsync(Form.Email, Form.Password, true, config.Value.EnableAccountLockout);
            if(result.IsLockedOut)
            {
                Form.Errors.Add("Too many failed login attempts. This account has been locked out.");
                return Page();
            }

            if(result.IsNotAllowed)
            {
                Form.Errors.Add("This email hasn't been confirmed yet.");
                return Page();
            }

            if(!result.Succeeded)
            {
                Form.Errors.Add("Email and password do not match");
                return Page();
            }

            return Redirect(string.IsNullOrWhiteSpace(returnUrl) ? "/" : returnUrl);
        }
    }
}