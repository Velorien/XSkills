using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using XSkills.Web.Models;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Options;

namespace XSkills.Web.Pages.Account
{
    [AllowAnonymous]
    public class ResetPasswordModel : PageModel
    {
        private readonly UserManager<XSkillsUser> userManager;
        private readonly IOptions<XSkillsConfig> config;

        public ResetPasswordModel(UserManager<XSkillsUser> userManager, IOptions<XSkillsConfig> config)
        {
            this.userManager = userManager;
            this.config = config;
        }

        [BindProperty(SupportsGet = true)]
        public ResetPasswordFormModel Form { get; set; } = new ResetPasswordFormModel();

        public void OnGet(string email, string token)
        {
            Form = new ResetPasswordFormModel { Email = email, Token = token };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Form.Errors.Add(ModelState.First(x => x.Value.Errors.Any()).Value.Errors.First().ErrorMessage);
                return Page();
            }

            var user = await userManager.FindByEmailAsync(Form.Email);
            if(user != null)
            {
                var result = await userManager.ResetPasswordAsync(user, Form.Token, Form.Password);
                if(result.Succeeded)
                {
                    Form.IsSuccess = true;
                    return Page();
                }

                Form.Errors.AddRange(result.Errors.Select(x => x.Description));
            }
            else
            {
                Form.Errors.Add(ResetPasswordFormModel.GenericError);
            }

            return Page();
        }
    }
}