using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using XSkills.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using Serilog;
using Microsoft.EntityFrameworkCore;

namespace XSkills.Web.Pages.Account
{
    [Authorize(Roles = "superadmin,admin")]
    public class AdminModel : PageModel
    {
        private readonly UserManager<XSkillsUser> userManager;
        private readonly RoleManager<XSkillsRole> roleManager;
        private readonly XSkillsDbContext db;

        public AdminModel(UserManager<XSkillsUser> userManager, RoleManager<XSkillsRole> roleManager, SignInManager<XSkillsUser> signInManager, XSkillsDbContext db)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.db = db;
        }

        public IEnumerable<UserFormModel> Users { get; set; }

        [BindProperty]
        public int UserId { get; set; }

        [BindProperty]
        public string Query { get; set; }

        public string Error { get; set; }

        public bool IsSuccess { get; set; }

        public async Task OnGetAsync()
        {
            await Initialize();
        }

        public async Task<IActionResult> OnPostAdminAsync()
        {
            var user = await GetUserAsync(UserId);
            if (user == null) return Redirect("/Account/LogOut");

            try
            {
                var roles = await userManager.GetRolesAsync(user);
                if (roles.Contains(MagicStrings.SuperAdmin) || !User.IsInRole(MagicStrings.SuperAdmin))
                {
                    Error = "You are not allowed to do that.";
                    await Initialize();
                    return Page();
                }
                else if (roles.Contains(MagicStrings.Admin))
                {
                    var result = await userManager.RemoveFromRoleAsync(user, MagicStrings.Admin);
                    IsSuccess = result.Succeeded;
                }
                else
                {
                    var result = await userManager.AddToRoleAsync(user, MagicStrings.Admin);
                    IsSuccess = result.Succeeded;
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex, "Error occurred when granting admin rights");
            }

            await Initialize();
            if (!IsSuccess) Error = $"Role change failed for user {user.Email}.";
            return Page();
        }

        public async Task OnPostRemoveAsync()
        {
            var user = await GetUserAsync(UserId);
            if (user == null) return;

            var roles = await userManager.GetRolesAsync(user);
            if (roles.Any(x => x == MagicStrings.SuperAdmin || x == MagicStrings.Admin))
            {
                Error = "You cannot remove admin accounts.";
                await Initialize();
                return;
            }

            var result = await userManager.DeleteAsync(user);
            IsSuccess = result.Succeeded;
            if (!IsSuccess) Error = $"Could not delete user {user.Email}.";
            await Initialize();
        }

        public async Task<IActionResult> OnPostSuperAdminAsync()
        {
            var user = await GetUserAsync(UserId);
            if (user == null) return Page();
            var currentUser = await userManager.GetUserAsync(User);

            if (user.Id == currentUser.Id || !User.IsInRole(MagicStrings.SuperAdmin))
            {
                await Initialize();
                Error = "Something went wrong.";
                return Page();
            }

            var results = new List<IdentityResult>();
            try
            {
                if(await userManager.IsInRoleAsync(user, MagicStrings.Admin))
                    results.Add(await userManager.RemoveFromRoleAsync(user, MagicStrings.Admin));
                results.Add(await userManager.AddToRoleAsync(user, MagicStrings.SuperAdmin));
                results.Add(await userManager.AddToRoleAsync(currentUser, MagicStrings.Admin));
                results.Add(await userManager.RemoveFromRoleAsync(currentUser, MagicStrings.SuperAdmin));

                IsSuccess = results.All(x => x.Succeeded);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error occurred when passing super admin rights");
                Error = $"Role change failed for user {user.Email}.";
                return Page();
            }

            if (!IsSuccess)
            {
                await Initialize();
                Error = string.Join(Environment.NewLine, results.Where(x => x != null && !x.Succeeded)
                    .Select(x => string.Join(Environment.NewLine, x.Errors.Select(y => y.Description))));
                return Page();
            }

            return Redirect("/Account/LogOut");
        }

        private async Task Initialize()
        {
            var adminRole = await roleManager.FindByNameAsync(MagicStrings.Admin);
            var superAdminRole = await roleManager.FindByNameAsync(MagicStrings.SuperAdmin);
            Users = from u in db.Users.Include(u => u.Employee)
                    join r in db.UserRoles on u.Id equals r.UserId into userRoles
                    from ur in userRoles.DefaultIfEmpty()
                    select UserFormModel.FromUser(u, ur.RoleId == superAdminRole.Id ? 2 : ur.RoleId == adminRole.Id ? 1 : 0);
        }

        private async Task<XSkillsUser> GetUserAsync(int id)
        {
            var user = await userManager.FindByIdAsync(UserId.ToString());
            if (user == null)
            {
                Error = "There is no such user.";
                await Initialize();
            }

            return user;
        }
    }
}