using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using XSkills.Web.Models;

namespace XSkills.Web.Pages.Account
{
    public class LogOutModel : PageModel
    {
        private readonly SignInManager<XSkillsUser> signInManager;

        public LogOutModel(SignInManager<XSkillsUser> signInManager)
        {
            this.signInManager = signInManager;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await signInManager.SignOutAsync();
            return Redirect("/Account/LogIn?ReturnUrl=/");
        }
    }
}