using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;

namespace XSkills.Web.Pages
{
    [AllowAnonymous]
    public class ErrorModel : PageModel
    {
        public string ErrorMessage { get; set; }

        public void OnGet(int? statusCode)
        {
            switch(statusCode)
            {
                case 404:
                    ErrorMessage = "The requested page could not be found.";
                    break;
                case 500:
                    ErrorMessage = "Server error occured. Go tell your admin to check the logs.";
                    break;
                case null:
                default:
                    ErrorMessage = "Something went wrong.";
                    break;
            }
        }
    }
}
