﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using XSkills.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using XSkills.Web.Helpers;
using Serilog;

namespace XSkills.Web.Pages
{
    [Authorize(Roles = "superadmin,admin")]
    public class ProjectsModel : PageModel
    {
        private readonly XSkillsDbContext db;

        public ProjectsModel(XSkillsDbContext db)
        {
            this.db = db;
        }

        [BindProperty]
        public int SelectedProjectId { get; set; }

        public List<SelectProjectFormModel> Projects { get; } = new List<SelectProjectFormModel>();

        public List<Tag> Tags { get; set; } = new List<Tag>();

        [BindProperty]
        public ProjectFormModel CurrentProject { get; set; } = new ProjectFormModel();

        public void OnGet() => Initialize();

        public void OnPostSelect()
        {
            Initialize();
            var project = db.Projects.Include(x => x.TagToProjects).FirstOrDefault(x => x.Id == SelectedProjectId);
            if (project != null)
            {
                CurrentProject = new ProjectFormModel
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    Tags = project.TagToProjects,
                    IsRetired = project.IsRetired,
                    LiveUrl = project.LiveUrl
                };
            }
        }

        public void OnPostProject()
        {
            this.ClearModelState();
            if(!ModelState.IsValid)
            {
                CurrentProject.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                Initialize();
                return;
            }

            var project = new Project
            {
                Id = CurrentProject.Id,
                Name = CurrentProject.Name,
                Description = CurrentProject.Description,
                IsRetired = CurrentProject.IsRetired,
                LiveUrl = CurrentProject.LiveUrl
            };

            if (CurrentProject.Id == 0)
            {
                db.Projects.Add(project);
            }
            else
            {
                db.Projects.Update(project);
            }

            try
            {
                db.SaveChanges();
                CurrentProject = new ProjectFormModel
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    IsRetired = project.IsRetired,
                    LiveUrl = project.LiveUrl
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Saving project failed");
                CurrentProject.Errors.Add("Could not update the project.");
            }

            Initialize();
        }

        private void Initialize()
        {
            Projects.AddRange(db.Projects.Select(x => new SelectProjectFormModel
            {
                Id = x.Id,
                Name = x.Name,
                IsRetired = x.IsRetired
            }).ToList());

            Tags.AddRange(db.Tags);
        }
    }
}