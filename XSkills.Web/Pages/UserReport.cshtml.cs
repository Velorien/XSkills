using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using XSkills.Web.Models;

namespace XSkills.Web.Pages
{
    public class UserReportModel : PageModel
    {
        private readonly XSkillsDbContext db;

        public List<XSkillsUser> Users { get; set; }

        public UserReportModel(XSkillsDbContext db)
        {
            this.db = db;
        }

        public IActionResult OnGet(string userIds)
        {
            try
            {
                var ids = userIds.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x));
                Users = db.Users.Include(x => x.UserToProjects).ThenInclude(x => x.Project)
                .Include(x => x.Employee).ThenInclude(x => x.Skills).ThenInclude(x => x.Skill).ThenInclude(x => x.SkillGroup)
                .Where(x => ids.Contains(x.Id)).ToList();
            }
            catch (Exception ex)
            {
                return Redirect("/Error/500");
            }

            return Page();
        }
    }
}