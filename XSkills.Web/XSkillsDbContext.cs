﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using XSkills.Web.Models;

namespace XSkills.Web
{
    public class XSkillsDbContext : IdentityDbContext<XSkillsUser, XSkillsRole, int>
    {
        public XSkillsDbContext(DbContextOptions<XSkillsDbContext> options) : base(options) { }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<EmployeeSkill> EmployeeSkills { get; set; }

        public DbSet<Skill> Skills { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<TagToSkill> TagToSkills { get; set; }

        public DbSet<SkillGroup> SkillGroups { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<TagToProject> TagToProjects { get; set; }

        public DbSet<UserToProject> UserToProjects { get; set; }

        public DbSet<SkillChange> SkillChanges { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<EmployeeSkill>(entity =>
                entity.HasIndex(e => new { e.EmployeeId, e.SkillId }).IsUnique());

            builder.Entity<Skill>(entity =>
                entity.HasIndex(e => new { e.Name, e.SkillGroupId }).IsUnique());

            builder.Entity<TagToSkill>(entity => 
            {
                entity.HasKey(e => new { e.TagId, e.SkillId });

                entity.HasOne(e => e.Skill)
                    .WithMany(e => e.TagToSkills)
                    .HasForeignKey(e => e.SkillId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(e => e.Tag)
                    .WithMany(e => e.TagToSkills)
                    .HasForeignKey(e => e.TagId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<TagToProject>(entity =>
            {
                entity.HasKey(e => new { e.TagId, e.ProjectId });

                entity.HasOne(e => e.Project)
                    .WithMany(e => e.TagToProjects)
                    .HasForeignKey(e => e.ProjectId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(e => e.Tag)
                    .WithMany(e => e.TagToProjects)
                    .HasForeignKey(e => e.TagId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<UserToProject>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ProjectId });

                entity.HasOne(e => e.Project)
                    .WithMany(e => e.UserToProjects)
                    .HasForeignKey(e => e.ProjectId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(e => e.User)
                    .WithMany(e => e.UserToProjects)
                    .HasForeignKey(e => e.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<SkillGroup>(entity =>
                entity.HasIndex(e => e.Name).IsUnique());

            builder.Entity<Tag>(entity =>
                entity.HasIndex(e => e.Name).IsUnique());

            builder.Entity<Project>(entity =>
                entity.HasIndex(e => e.Name).IsUnique());

            builder.Entity<SkillChange>(entity => {
                entity.HasOne(e => e.EmployeeSkill)
                    .WithMany(e => e.Archive)
                    .HasForeignKey(e => e.EmployeeSkillId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Employee>(entity =>
            {
                entity.HasOne(e => e.User)
                    .WithOne(e => e.Employee)
                    .HasForeignKey<Employee>(e => e.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}
