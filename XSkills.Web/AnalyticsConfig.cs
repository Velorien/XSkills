﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using XSkills.Web.Helpers;
using XSkills.Web.Services;

namespace XSkills.Web
{
    public static class AnalyticsConfig
    {
        public static AnalyticsService<XSkillsDbContext> Configure()
        {
            var service = new AnalyticsService<XSkillsDbContext>();

            service.RegisterMap("Employee tags",
                x => x.Employees.Include(e => e.User).Include(e => e.Skills).ThenInclude(e => e.Skill).ThenInclude(e => e.TagToSkills).ThenInclude(e => e.Tag).ToDictionary(
                    k => k.User.Email,
                    v => v.Skills.SelectMany(y => y.Skill.TagToSkills.Select(z => z.Tag.Name)).Distinct()),
                DisplayType.List);

            service.RegisterMapSingle("Average skill level",
                x => x.Skills.Include(e => e.EmployeeSkills).Where(e => e.EmployeeSkills.Any()).ToDictionary(
                    k => k.Name,
                    v => v.EmployeeSkills.Average(y => y.ProficiencyLevel).ToString("F")),
                DisplayType.BarChart);

            service.RegisterMapSingle("Average skill experience",
                x => x.Skills.Include(e => e.EmployeeSkills).Where(e => e.EmployeeSkills.Any()).ToDictionary(
                    k => k.Name,
                    v => v.EmployeeSkills.Average(y => y.YearsOfExperience).ToString("F")),
                DisplayType.BarChart);

            service.RegisterMap("Employees with skill",
                x => x.Skills.Include(e => e.EmployeeSkills).ThenInclude(e => e.Employee).ThenInclude(e => e.User).ToDictionary(
                    k => k.Name,
                    v => v.EmployeeSkills.Select(y => $"{y.Employee.User.Email} (Level {y.ProficiencyLevel})")),
                DisplayType.List);

            service.RegisterMap("Employees with skill > 3",
                x => x.Skills.Include(e => e.EmployeeSkills).ThenInclude(e => e.Employee).ThenInclude(e => e.User).ToDictionary(
                    k => k.Name,
                    v => v.EmployeeSkills.Where(y => y.ProficiencyLevel > 3).Select(y => $"{y.Employee.User.Email} (Level {y.ProficiencyLevel})")),
                DisplayType.List);

            service.RegisterMap("Skill proficiency distribution",
                x => x.Skills.Include(e => e.EmployeeSkills).ToDictionary(
                    k => k.Name,
                    v => v.EmployeeSkills.GroupBy(y => y.ProficiencyLevel).Select(y => $"Level {y.Key} ({y.Count()}):{y.Count()}")),
                DisplayType.PieChart);

            service.RegisterMap("Skills with tag",
                x => x.Tags.Include(e => e.TagToSkills).ThenInclude(e => e.Skill).ToDictionary(
                    k => k.Name,
                    v => v.TagToSkills.Select(y => y.Skill.Name)),
                DisplayType.List);

            service.RegisterMap("Skills by relevance",
                x => x.Skills.GroupBy(e => e.Relevance).ToDictionary(
                    k => k.Key == 2 ? "High relevance" : k.Key == 1 ? "Medium relevance" : "Low relevance",
                    v => v.Select(y => y.Name)),
                DisplayType.List);

            service.RegisterMapSingle("Tag skill count",
                x => x.Tags.Include(e => e.TagToSkills).OrderByDescending(e => e.TagToSkills.Count).ToDictionary(
                    k => k.Name,
                    v => v.TagToSkills.Count().ToString()),
                DisplayType.BarChart);

            service.RegisterMapSingle("Group skill count",
                x => x.SkillGroups.Include(e => e.Skills).OrderByDescending(e => e.Skills.Count).ToDictionary(
                    k => k.Name,
                    v => v.Skills.Count().ToString()),
                DisplayType.BarChart);

            service.RegisterMapSingle("Employees with skill count",
                x => x.Skills.Include(e => e.EmployeeSkills).ToDictionary(
                    k => k.Name,
                    v => v.EmployeeSkills.Count().ToString()),
                DisplayType.BarChart);

            service.RegisterMapSingle("Skills by relevance count",
                x => x.Skills.GroupBy(e => e.Relevance).ToDictionary(
                    k => k.Key == 2 ? "High relevance" : k.Key == 1 ? "Medium relevance" : "Low relevance",
                    v => v.Count().ToString()),
                DisplayType.PieChart);

            service.RegisterMap("Employees by skill update time",
                x => x.Employees.Include(e => e.User).OrderBy(e => e.LastSkillUpdate).GroupBy(e => e.LastSkillUpdate.ToMonthsAgo()).ToDictionary(
                    k => k.Key,
                    v => v.Select(y => $"{y.User.Email}")),
                DisplayType.List);

            return service;
        }
    }
}
