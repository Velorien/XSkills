﻿Vue.component('xskills-search', {
    template: '#xskills-search-template',
    data: function () {
        return {
            name: '',
            timer: null,
            employees: [],
            errors: [],
            displayState: { intro: 'intro', search: 'search', noResults: 'norResults', error: 'error', loading: 'loading' },
            currentState: 'intro',
            projects: [],
            timeout: null
        };
    },
    methods: {
        selectEmployee: function (id) {
            this.$emit('employeeselected', id);
        }
    },
    mounted: function () {
        var t = this;
        fetch('/api/Projects', { method: 'GET', credentials: 'include' })
            .then(r => {
                return r.json().then(d => {
                    if (r.ok) {
                        t.projects = d;
                    }
                });
            });
    },
    watch: {
        name: function () {
            var t = this;
            t.errors = [];
            clearTimeout(t.timeout);
            if (!t.name || t.name === '') {
                t.employees = [];
                t.currentState = t.displayState.intro;
                return;
            }

            t.timeout = setTimeout(() => {
                t.currentState = t.displayState.loading;
                fetch('/api/Employees/Search/' + t.name, { method: 'GET', credentials: 'include' })
                    .then(r => {
                        return r.json().then(d => {
                            if (r.ok) {
                                t.employees = d;
                                if (t.employees.length > 0)
                                    t.currentState = t.displayState.search;
                                else
                                    t.currentState = t.displayState.noResults;
                            } else {
                                t.currentState = t.displayState.error;
                                t.errors = d;
                            }
                        });
                    });
            }, 500);
        }
    }
});