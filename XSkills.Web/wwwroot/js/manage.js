﻿Vue.component('xskills-manage', {
    template: '#xskills-manage-template',
    data: function () {
        return {
            currentCategory: {},
            categories: [
                {
                    name: 'Skills',
                    nameSingular: 'Skill',
                    route: 'Skills'
                },
                {
                    name: 'Skill groups',
                    nameSingular: 'Skill group',
                    route: 'SkillGroups'
                },
                {
                    name: 'Tags',
                    nameSingular: 'Tag',
                    route: 'Tags'
                }
            ],
            hideMenu: true,
            category: { skills: 'Skills', tags: 'Tags', skillGroups: 'SkillGroups', loading: 'Loading' },
            skills: [],
            relevance: [{ value: 0, name: 'Low' }, { value: 1, name: 'Medium' }, { value: 2, name: 'High' }],
            tags: [],
            skillGroups: [],
            errors: [],
            successMessage: null,
            oldName: null,
            loaded: 0,
            current: { name: '', id: 0 },
            currentInternal: {},
            selectedRelevance: 1,
            selectedSkillGroup: {},
            tagQuery: '',
            tagsFiltered: [],
            itemToDelete: null,
            query: ''
        };
    },
    methods: {
        add: function () {
            var t = this;
            t.errors = [];
            fetch('/api/' + t.currentCategory.route, {
                method: 'POST',
                credentials: 'include',
                headers: { 'Content-type': 'application/json' },
                body: t.currentCategory.route === t.category.skills ?
                    JSON.stringify({
                        name: t.current.name,
                        skillGroupId: t.selectedSkillGroup.id,
                        relevance: t.selectedRelevance
                    }) :
                    JSON.stringify({ name: t.current.name })
            }).then(r => {
                return r.json().then(d => {
                    if (r.ok) {
                        switch (t.currentCategory.route) {
                            case t.category.skills:
                                t.skills.push({
                                    name: t.current.name,
                                    skillGroup: t.selectedSkillGroup,
                                    relevance: t.selectedRelevance,
                                    tagToSkills: [],
                                    id: d
                                });
                                break;
                            case t.category.skillGroups:
                                t.skillGroups.push({ name: t.current.name, id: d });
                                break;
                            case t.category.tags:
                                t.tags.push({ name: t.current.name, id: d });
                                break;
                        }

                        t.current = { name: '', id: 0 };
                        t.displaySuccessMessage('added');
                    }
                    else if (r.status === 409) {
                        t.errors.push(t.current.name + ': ' + t.currentCategory.nameSingular + ' already exists.');
                    } else t.errors = d;
                });
            });
        },
        update: function () {
            var t = this;
            if (!t.current.id) return;
            t.errors = [];
            fetch('/api/' + t.currentCategory.route, {
                method: 'PUT',
                credentials: 'include',
                headers: { 'Content-type': 'application/json' },
                body: t.currentCategory.route === t.category.skills ?
                    JSON.stringify({
                        name: t.current.name,
                        id: t.current.id,
                        skillGroupId: t.selectedSkillGroup.id,
                        relevance: t.selectedRelevance
                    }) :
                    JSON.stringify(t.current)
            }).then(r => {
                return r.json().then(d => {
                    if (r.ok) {
                        t.currentInternal.name = t.current.name;
                        switch (t.currentCategory.route) {
                            case t.category.skills:
                                t.currentInternal.skillGroup = t.selectedSkillGroup;
                                t.currentInternal.relevance = t.selectedRelevance;
                                break;
                            case t.category.skillGroups:
                                for (s of t.skills) if (s.skillGroup.id === t.current.id) s.skillGroup.name = t.current.name;
                                break;
                        }

                        t.current = { name: '', id: 0 };
                        t.currentInternal = {};
                        t.displaySuccessMessage('updated');
                    } else {
                        if (r.status === 409) {
                            t.errors.push(t.current.name + ': ' + t.currentCategory.nameSingular + ' already exists.');
                        } else {
                            throw Error(r.json());
                        }
                    }
                });
            });
        },
        remove: function (id) {
            var t = this;
            t.errors = [];
            fetch('/api/' + t.currentCategory.route + '/' + id, {
                credentials: 'include',
                method: 'DELETE'
            }).then(r => {
                if (r.ok) {
                    switch (t.currentCategory.route) {
                        case t.category.skills:
                            for ([i, s] of t.skills.entries()) if (s.id === id) {
                                t.skills.splice(i, 1); break;
                            }
                            break;
                        case t.category.tags:
                            for (sk of t.skills) {
                                for ([i, s] of sk.tagToSkills.entries()) if (s.tag.id === id) {
                                    sk.tagToSkills.splice(i, 1); break;
                                }
                            }
                            for ([i, st] of t.tags.entries()) if (st.id === id) {
                                t.tags.splice(i, 1);
                            }
                            break;
                        case t.category.skillGroups:
                            for ([i, s] of t.skillGroups.entries()) if (s.id === id) {
                                t.skillGroups.splice(i, 1); break;
                            }
                            for (s of t.skills) if (s.skillGroup.id === id) s.skillGroup = { name: 'Unassigned', id: 1 };
                            break;
                    }

                    if (t.current.id === id) {
                        t.currentInternal = {};
                        t.current = { name: '', id: 0 };
                    }

                    t.displaySuccessMessage('deleted');
                } else t.errors.push('Could not delete ' + t.currentCategory.nameSingular);
            });
        },
        displaySuccessMessage: function (verb) {
            var t = this;
            t.successMessage = t.currentCategory.nameSingular + ' has been ' + verb + ' successfully.';
            setTimeout(() => { t.successMessage = null; }, 2000);
        },
        select: function (item) {
            var t = this;
            t.currentInternal = item;
            t.current.tags = [];
            if (t.currentCategory.route === t.category.skills) {
                t.selectedSkillGroup = item.skillGroup;
                t.selectedRelevance = item.relevance;
                t.current.tagToSkills = item.tagToSkills;
            }

            t.current.name = item.name;
            t.current.id = item.id;
        },
        tag: function (tag) {
            var t = this;
            if (!t.current.id) return;
            fetch('/api/Skills/Tag/' + t.current.id + '/' + tag.id, {
                credentials: 'include',
                method: 'GET'
            }).then(r => {
                return r.json().then(d => {
                    if (r.ok) {
                        t.current.tagToSkills.push({ tag: tag, skillId: t.current.id });
                    }
                });
            });
        },
        untag: function (tag) {
            var t = this;
            fetch('/api/Skills/Untag/' + tag.skillId + '/' + tag.tag.id, {
                credentials: 'include',
                method: 'GET'
            }).then(r => {
                return r.json().then(d => {
                    if (r.ok) {
                        for (s of t.skills) {
                            if (s.id === tag.skillId) {
                                var index = s.tagToSkills.indexOf(tag);
                                s.tagToSkills.splice(index, 1);
                                return;
                            }
                        }
                    }
                });
            });
        },
        matchesQuery: function (item) {
            return item.name.toLowerCase().indexOf(this.query.toLowerCase()) !== -1;
        },
        addSkill: function (skill) {
            var t = this;
            t.errors = [];
            fetch('/api/EmployeeSkills', {
                method: 'POST',
                credentials: 'include',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify({ skillId: skill.id, yearsOfExperience: 0, proficiencyLevel: 0, employeeId })
            }).then(r => {
                if (r.ok) {
                    skill.isOwned = true;
                    t.successMessage = 'Skill added successfully.';
                    setTimeout(() => { t.successMessage = null; }, 2000);
                }
                else {
                    t.errors.push('Could not add a skill.')
                }
            });
        }
    },
    watch: {
        currentCategory: function () {
            var t = this;
            t.successMessage = null;
            t.errors = [];
            t.current = { name: '', id: 0 };
        },
        tagQuery: function () {
            var t = this;
            if (t.tagQuery === '') {
                t.tagsFiltered = [];
                return;
            }

            var tags = [];
            var lowercase = t.tagQuery.toLowerCase();
            for (st of t.tags) if (st.name.toLowerCase().indexOf(lowercase) !== -1) tags.push(st);
            t.tagsFiltered = tags;
        }
    },
    mounted: function () {
        var t = this;
        fetch('/api/EmployeeSkills/OwnedSkillIds', { credentials: 'include', method: 'GET' })
            .then(res => { if (res.ok) return res.json(); })
            .then(ids => {
                fetch('/api/Skills', { credentials: 'include', method: 'GET'})
                    .then(r => { if (r.ok) return r.json(); })
                    .then(d => {
                        for (s of d) s.isOwned = ids.indexOf(s.id) > -1;
                        t.skills = d; t.loaded++;
                    });
            });
        fetch('/api/Tags', { credentials: 'include', method: 'GET' })
            .then(r => { if (r.ok) return r.json(); })
            .then(d => {
                t.tags = d;
                t.loaded++;
            });
        fetch('/api/SkillGroups', { credentials: 'include', method: 'GET' })
            .then(r => { if (r.ok) return r.json(); })
            .then(d => {
                t.skillGroups = d;
                t.selectedSkillGroup = t.skillGroups[0];
                t.loaded++;
            });
        t.currentCategory = t.categories[0];
    }
});