﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace XSkills.Web.Services
{
    public class AnalyticsService<TDbContext> where TDbContext : DbContext
    {
        private readonly Dictionary<string, AnalyticsDefinition> registrations
            = new Dictionary<string, AnalyticsDefinition>();

        public void RegisterMap(
            string name, Expression<Func<TDbContext, Dictionary<string, IEnumerable<string>>>> expression, DisplayType displayType) =>
            registrations.Add(name, new AnalyticsDefinition(name, expression, displayType));

        public void RegisterMapSingle(
            string name, Expression<Func<TDbContext, Dictionary<string, string>>> expression, DisplayType displayType) =>
            registrations.Add(name, new AnalyticsDefinition(name, expression, displayType));

        public AnalyticsDefinition GetMap(string name) => registrations.Values.SingleOrDefault(x => x.Name == name);

        public IEnumerable<AnalyticsMapInfo> GetMapsInfo() => registrations.Select(x => new AnalyticsMapInfo { Name = x.Key, DisplayType = x.Value.DisplayType });
    }

    public class AnalyticsMapInfo
    {
        public string Name { get; set; }

        public DisplayType DisplayType { get; set; }
    }

    public class AnalyticsDefinition
    {
        private readonly Delegate expression;
        private readonly bool isSingle;
        private string nameCamelCase;

        public AnalyticsDefinition(string name, LambdaExpression expression, DisplayType displayType)
        {
            isSingle = !expression.Type.GetGenericArguments()[1].GetGenericArguments()[1].IsGenericType;
            this.expression = expression.Compile();
            Name = name;
            DisplayType = displayType;
        }

        public string Name { get; }

        public DisplayType DisplayType { get; }

        public string NameCamelCase => nameCamelCase ?? (nameCamelCase = string.Join(string.Empty,
            Name.ToLower().Split(' ', StringSplitOptions.RemoveEmptyEntries)
            .Select((x, i) => i > 0 ? x.Length > 1 ? x.Substring(0, 1).ToUpper() + x.Substring(1) : x.ToUpper() : x)));

        public Dictionary<string, string> EvaluateSingle(DbContext argument) =>
            isSingle ? expression.DynamicInvoke(argument) as Dictionary<string, string> : null;

        public Dictionary<string, IEnumerable<string>> EvaluateCollection(DbContext argument) =>
            isSingle ? null : expression.DynamicInvoke(argument) as Dictionary<string, IEnumerable<string>>;
    }

    public enum DisplayType
    {
        List, BarChart, PieChart
    }
}
