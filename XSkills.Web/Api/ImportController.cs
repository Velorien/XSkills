﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using XSkills.Web.Models;

namespace XSkills.Web.Api
{
    [Route("api/Import")]
    public class ImportController : Controller
    {
        private readonly XSkillsDbContext db;
        public ImportController(XSkillsDbContext db)
        {
            this.db = db;
        }

        [HttpPost("{name}")]
        public IActionResult Route(string name)
        {
            if (name == "Employee") return NotFound();
            var type = Type.GetType("XSkills.Web.Models." + name);
            if (type == null) return NotFound();
            var method = typeof(ImportController).GetMethod("Import").MakeGenericMethod(type);
            string payload = null;
            using (var reader = new StreamReader(Request.Body))
            {
                payload = reader.ReadToEnd();
            }

            return method.Invoke(this, new[] { payload }) as IActionResult;
        }

        public IActionResult Import<T>(string json) where T : EntityBase
        {
            try
            {
                var data = JsonConvert.DeserializeObject<IEnumerable<T>>(json).ToList();
                var errors = new List<string>();
                for (int i = 0; i < data.Count(); i++)
                {
                    var context = new ValidationContext(data[i]);
                    var validationResults = new List<ValidationResult>();
                    if(!Validator.TryValidateObject(data[i], context, validationResults))
                    {
                        errors.AddRange(validationResults.Select(x => $"Index {i}: {x.ErrorMessage}"));
                    }
                }

                if (errors.Any()) return BadRequest(errors);

                db.Set<T>().AddRange(data);
                db.SaveChanges();
                return Ok(data.Select(x => x.Id));
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(new[] { ex.InnerException.Message });
            }
            catch (Exception ex)
            {
                return BadRequest(new[] { ex.Message });
            }
        }
    }
}
