﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using XSkills.Web.Models;

namespace XSkills.Web.Api
{
    public abstract class NamedEntityController<T> : CrudController<T> where T : NamedEntity
    {
        public NamedEntityController(XSkillsDbContext db) : base(db) { }

        public override IEnumerable<T> Get() => base.Get();

        [Authorize(Roles = "superadmin,admin")]
        public override IActionResult Post([FromBody] T item) => base.Post(item);

        [Authorize(Roles = "superadmin,admin")]
        public override IActionResult Put([FromBody] T item) => base.Put(item);

        [Authorize(Roles = "superadmin,admin")]
        public override IActionResult Delete(int id) => base.Delete(id);
    }

    [Route("api/SkillGroups")]
    public class SkillGroupsController : NamedEntityController<SkillGroup>
    {
        public SkillGroupsController(XSkillsDbContext db) : base(db) { }

        public override IActionResult Put([FromBody] SkillGroup item)
        {
            if (item.Id == 1) return StatusCode(403, new[] { "Cannot change the Unassigned group" });
            return base.Put(item);
        }

        public override IActionResult Delete(int id)
        {
            if (id == 1) return StatusCode(403, new[] { "Cannot delete the Unassigned group" });
            var skills = db.Skills.Where(x => x.SkillGroupId == id);
            foreach (var s in skills) s.SkillGroupId = 1;
            db.UpdateRange(skills);
            return base.Delete(id);
        }
    }

    [Route("api/Tags")]
    public class TagsController : NamedEntityController<Tag>
    {
        public TagsController(XSkillsDbContext db) : base(db) { }
    }
}
