﻿using Microsoft.AspNetCore.Mvc;
using XSkills.Web.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using Serilog;
using Microsoft.AspNetCore.Authorization;

namespace XSkills.Web.Api
{
    [Route("api/Skills")]
    public class SkillsController : CrudController<Skill>
    {
        public SkillsController(XSkillsDbContext db) : base(db) { }

        [Authorize(Roles = "superadmin,admin")]
        public override IActionResult Put([FromBody] Skill item) => base.Put(item);

        [Authorize(Roles = "superadmin,admin")]
        public override IActionResult Post([FromBody] Skill item) => base.Post(item);

        [Authorize(Roles = "superadmin,admin")]
        public override IActionResult Delete(int id) => base.Delete(id);

        public override IEnumerable<Skill> Get() => db.Skills.Include(x => x.SkillGroup).Include(x => x.TagToSkills).ThenInclude(x => x.Tag).OrderBy(x => x.Name.ToLower()).ToList();

        [HttpGet("Tag/{skillId}/{tagId}")]
        [Authorize(Roles = "superadmin,admin")]
        public IActionResult Tag(int skillId, int tagId)
        {
            try
            {
                db.TagToSkills.Add(new TagToSkill { SkillId = skillId, TagId = tagId });
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tagging skill failed");
                return BadRequest(new[] { $"Could not tag skillId {skillId} with tagId {tagId}" });
            }

            return Ok((skillId, tagId));
        }

        [HttpGet("Untag/{skillId}/{tagId}")]
        [Authorize(Roles = "superadmin,admin")]
        public IActionResult Untag(int skillId, int tagId)
        {
            try
            {
                db.TagToSkills.Remove(new TagToSkill { SkillId = skillId, TagId = tagId });
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Untagging skill failed");
                return BadRequest(new[] { $"Could not untag skillId {skillId} with tagId {tagId}" });
            }

            return Ok((skillId, tagId));
        }
    }
}
