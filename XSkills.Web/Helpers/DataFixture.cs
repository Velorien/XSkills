﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using XSkills.Web.Models;
using static XSkills.Web.Helpers.RandomHelpers;

namespace XSkills.Web.Helpers
{
    public static class DataFixture
    {
        public static void LoadData(XSkillsDbContext db, string contentRootPath)
        {
            string path = Path.Combine(contentRootPath, "sample.json");
            var data = LoadFromFile(path);
            db.Tags.AddRange(GenerateNamedEntities<Tag>(data.Tags));
            db.SkillGroups.AddRange(GenerateNamedEntities<SkillGroup>(data.SkillGroups));
            db.SaveChanges();
            db.Skills.AddRange(GenerateSkills(data.Skills, data.SkillGroups.Count() + 1));
            db.SaveChanges();
            var tagIds = db.Tags.Select(x => x.Id);
            foreach (var skill in db.Skills)
            {
                var ids = tagIds.ToList().Shuffle().Take(Rng.Next(2, 6));
                skill.TagToSkills = ids.Select(x => new TagToSkill { TagId = x, Skill = skill }).ToList();
            }

            foreach (var user in data.Users)
            {
                user.SecurityStamp = Guid.NewGuid().ToString();
                user.UserName = user.Email;
                user.Avatar = MagicStrings.UnknownAvatar;
            }

            db.Users.AddRange(data.Users);
            db.SaveChanges();
            var employees = data.Users.Select(x => new Employee { User = x });
            db.Employees.AddRange(employees);
            db.SaveChanges();
            var skillIds = db.Skills.Select(x => x.Id).ToList();
            foreach (var employee in db.Employees)
            {
                var ids = skillIds.ToList().Shuffle().Take(Rng.Next(7, 15));
                employee.Skills = ids.Select(x => new EmployeeSkill
                {
                    SkillId = x,
                    Employee = employee,
                    YearsOfExperience = Rng.Next(1, 11),
                    ProficiencyLevel = Rng.Next(1, 6)
                }).ToList();
            }
            db.SaveChanges();
        }

        private static DataFixtureModel LoadFromFile(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<DataFixtureModel>(json);
        }

        private static IEnumerable<T> GenerateNamedEntities<T>(IEnumerable<string> entities) where T : NamedEntity, new()
        {
            foreach (var item in entities)
            {
                var result = new T { Name = item };
                yield return result;
            }
        }

        private static IEnumerable<Skill> GenerateSkills(IEnumerable<string> skills, int skillGroupsCount)
        {
            foreach (var skill in skills)
            {
                var result = new Skill
                {
                    Name = skill,
                    Relevance = Rng.Next(3),
                    SkillGroupId = Rng.Next(1, skillGroupsCount + 1),
                };

                yield return result;
            }
        }

    }

    public class DataFixtureModel
    {
        public IEnumerable<string> Skills { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IEnumerable<string> SkillGroups { get; set; }
        public IEnumerable<XSkillsUser> Users { get; set; }
    }
}
