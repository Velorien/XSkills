﻿using System;
using System.Collections.Generic;

namespace XSkills.Web.Helpers
{
    public static class RandomHelpers
    {
        public static Random Rng { get; } = new Random();

        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            var rng = new Random();

            int n = list.Count;
            while (n-- > 1)
            {
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

        public static double RandomNormal(double mean, double stdDev) =>
            mean + stdDev * Math.Sqrt(-2.0 * Math.Log(1.0 - Rng.NextDouble())) *
                Math.Sin(2.0 * Math.PI * (1.0 - Rng.NextDouble()));

        public static double RandomNormal() => RandomNormal(0, 1);
    }
}
