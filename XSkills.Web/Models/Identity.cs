﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XSkills.Web.Models
{
    public class XSkillsUser : IdentityUser<int>
    {
        public XSkillsUser()
        {
            UserToProjects = new List<UserToProject>();
        }

        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        public string Avatar { get; set; }

        public string Description { get; set; }

        [JsonIgnore]
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }

        public List<UserToProject> UserToProjects { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }
    }

    public class XSkillsRole : IdentityRole<int> { }

    public enum AssignmentStatus
    {
        Unknown,
        Unassigned,
        Internal,
        CommercialNotMonetized,
        Commercial
    }
}
