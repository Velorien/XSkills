﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XSkills.Web.Models
{
    public abstract class FormModelBase
    {
        public bool IsSuccess { get; set; }

        public List<string> Errors { get; set; } = new List<string>();
    }

    public abstract class EmailFormModelBase : FormModelBase
    {
        [Required(ErrorMessage = "Email is required."), EmailAddress(ErrorMessage = "You must provide a valid email address.")]
        public string Email { get; set; }
    }

    public class ChangeNameFormModel : FormModelBase
    {
        [Required(ErrorMessage = "First name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }
    }

    public class ChangePasswordFormModel : FormModelBase
    {
        [DataType(DataType.Password), Required(ErrorMessage = "Old password is required.")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password), Required(ErrorMessage = "New password is required."), MinLength(10, ErrorMessage = "Password must be at least 10 characters long.")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password), Compare("NewPassword", ErrorMessage = "Passwords do not match.")]
        public string RepeatPassword { get; set; }
    }

    public class ForgotPasswordFormModel : EmailFormModelBase { }

    public class ResendActivationLinkFormModel : EmailFormModelBase { }

    public class ChangeEmailFormModel : EmailFormModelBase { }

    public class LogInFormModel : EmailFormModelBase
    {
        [Required(ErrorMessage = "Password is required"), DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class ResetPasswordFormModel : FormModelBase
    {
        public const string GenericError = "Something went wrong. Check the reset link.";

        [DataType(DataType.Password), Required(ErrorMessage = "Password is required."), MinLength(10, ErrorMessage = "Password must be at least 10 characters long.")]
        public string Password { get; set; }

        [DataType(DataType.Password), Compare("Password", ErrorMessage = "Passwords do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = GenericError), EmailAddress(ErrorMessage = GenericError)]
        public string Email { get; set; }

        [Required(ErrorMessage = GenericError)]
        public string Token { get; set; }
    }

    public class SignUpFormModel : EmailFormModelBase
    {
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        [DataType(DataType.Password), Required(ErrorMessage = "Password is required."), MinLength(10, ErrorMessage = "Password must be at least 10 characters long.")]
        public string Password { get; set; }

        [DataType(DataType.Password), Compare("Password", ErrorMessage = "Passwords do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class SelectProjectFormModel : FormModelBase
    {
        [Required(ErrorMessage = "Project name is required"), MaxLength(50, ErrorMessage = "Project name cannot be longer than 50 characters.")]
        public string Name { get; set; }

        public int Id { get; set; }

        public bool IsRetired { get; set; }
    }

    public class ProjectFormModel : SelectProjectFormModel
    {
        [Required(ErrorMessage = "Project description is required")]
        public string Description { get; set; }

        public string LiveUrl { get; set; }

        public List<XSkillsUser> Employees { get; set; }

        public List<TagToProject> Tags { get; set; }
    }

    public class UserFormModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Avatar { get; set; }

        public string Email { get; set; }

        public int AdminLevel { get; set; }

        public DateTime? LastSkillUpdate { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }

        public static UserFormModel FromUser(XSkillsUser user, int adminLevel) => new UserFormModel
        {
            Id = user.Id,
            FirstName = user.FirstName,
            LastName = user.LastName,
            Email = user.Email,
            AdminLevel = adminLevel,
            Avatar = user.Avatar,
            LastSkillUpdate = user.Employee?.LastSkillUpdate,
            AssignmentStatus = (AssignmentStatus)user.AssignmentStatus
        };
    }
}
