﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XSkills.Web.Models
{
    public class Skill : NamedEntity
    {
        [Range(0, 2, ErrorMessage = "Relevance must be Low (0), Medium (1) or High (2)")]
        public int Relevance { get; set; }

        public SkillGroup SkillGroup { get; set; }
        public int SkillGroupId { get; set; }

        [JsonIgnore]
        public List<EmployeeSkill> EmployeeSkills { get; set; }
        public List<TagToSkill> TagToSkills { get; set; }
    }
}