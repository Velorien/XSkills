﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XSkills.Web.Models
{
    public class EmployeeSkill : EntityBase
    {
        public EmployeeSkill()
        {
            Archive = new List<SkillChange>();
        }

        public Skill Skill { get; set; }
        public int SkillId { get; set; }

        [JsonIgnore]
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }

        [Required, Range(0, 100, ErrorMessage = "You must have between 0 and 100 years of experience")]
        public double YearsOfExperience { get; set; }

        [Required, Range(0, 5, ErrorMessage = "Proficiency level must be between 0 and 5")]
        public double ProficiencyLevel { get; set; }

        public List<SkillChange> Archive { get; set; }
    }
}