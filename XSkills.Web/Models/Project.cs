﻿using System.Collections.Generic;

namespace XSkills.Web.Models
{
    public class Project : NamedEntity
    {
        public Project()
        {
            UserToProjects = new List<UserToProject>();
            TagToProjects = new List<TagToProject>();
        }

        public bool IsRetired { get; set; }

        public string Description { get; set; }

        public string LiveUrl { get; set; }

        public List<UserToProject> UserToProjects { get; set; }

        public List<TagToProject> TagToProjects { get; set; }
    }
}