﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XSkills.Web.Models
{
    public abstract class NamedEntity : EntityBase
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
    }

    public class SkillGroup : NamedEntity
    {
        public SkillGroup()
        {
            Skills = new List<Skill>();
        }

        [JsonIgnore]
        public List<Skill> Skills { get; set; }
    }

    public class Tag : NamedEntity
    {
        public Tag()
        {
            TagToSkills = new List<TagToSkill>();
            TagToProjects = new List<TagToProject>();
        }

        [JsonIgnore]
        public List<TagToSkill> TagToSkills { get; set; }
        [JsonIgnore]
        public List<TagToProject> TagToProjects { get; set; }
    }

    public class TagToSkill
    {
        [JsonIgnore]
        public Skill Skill { get; set; }
        public int SkillId { get; set; }

        public Tag Tag { get; set; }
        public int TagId { get; set; }
    }

    public class TagToProject
    {
        [JsonIgnore]
        public Project Project { get; set; }
        public int ProjectId { get; set; }

        public Tag Tag { get; set; }
        public int TagId { get; set; }
    }
}