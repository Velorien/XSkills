﻿using Newtonsoft.Json;

namespace XSkills.Web.Models
{
    public class UserToProject
    {
        [JsonIgnore]
        public Project Project { get; set; }
        public int ProjectId { get; set; }

        public XSkillsUser User { get; set; }
        public int UserId { get; set; }
    }
}