﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace XSkills.Web.Models
{
    public class Employee : EntityBase
    {
        public Employee()
        {
            Skills = new List<EmployeeSkill>();
        }

        public DateTime? LastSkillUpdate { get; set; }

        public List<EmployeeSkill> Skills { get; set; }

        public XSkillsUser User { get; set; }
        public int UserId { get; set; }

        [NotMapped]
        public string LastSkillUpdateTime => LastSkillUpdate?.ToString("yyyy-MM-dd hh:mm");

        [NotMapped]
        public bool IsStale => LastSkillUpdate.HasValue && (DateTime.Now - LastSkillUpdate.Value).TotalDays > 90;
    }

    public class NamedEmployee : Employee
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Avatar { get; set; }

        public string Email { get; set; }

        public AssignmentStatus AssignmentStatus { get; set; }

        public static NamedEmployee FromEmployee(Employee e) => new NamedEmployee
        {
            Id = e.Id,
            LastSkillUpdate = e.LastSkillUpdate,
            Skills = e.Skills,
            FirstName = e.User?.FirstName,
            LastName = e.User?.LastName,
            Avatar = e.User?.Avatar,
            Email = e.User?.Email,
            UserId = e.User?.Id ?? 0,
            AssignmentStatus = e.User?.AssignmentStatus ?? 0
        };
    }
}
